package com.zj.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger2 class
 * Implement configuration
 * @author zhangjun
 * @date 26/03/2018
 */
@Configuration
@EnableSwagger2
public class Swagger2 {
    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zj.demo.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo(){
        Contact contact = new Contact("jz","http://www.baidu.com/","offwhite1124@gmail.com");
        return new ApiInfoBuilder()
                .title("Swagger2")
                .description("Using Swagger2 to implement RESTful APIs in Spring Boot")
                .termsOfServiceUrl("http://www.baidu.com/")
                .contact(contact)
                .version("1.0.0")
                .build();
    }
}
