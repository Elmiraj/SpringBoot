package com.zj.demo.controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * HelloController class
 *
 * @author zhangjun
 * @date 26/03/2018
 */
@RestController
public class HelloController {
    @ApiIgnore
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String index(ModelMap modelMap){
        modelMap.addAttribute("host","http://www.baidu.com");
        return "index";
    }
}
