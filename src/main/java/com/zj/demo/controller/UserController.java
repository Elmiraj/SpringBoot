package com.zj.demo.controller;

import com.zj.demo.model.User;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import java.util.*;

/**
 * UserController class
 * Achieve the user's operation API
 * @author zhangjun
 * @date 26/03/2018
 */
@RestController
@RequestMapping(value = "/users")
public class UserController {
    /**
     * Generate thread security Map
     * */
    static Map<Long, User> userMap = Collections.synchronizedMap(new HashMap<Long, User>());

    @ApiOperation(value = "Get user info", notes = "Get user info")
    @RequestMapping(value = {""}, method = RequestMethod.GET)
    public List<User> getUserList(){
        List<User> values = new ArrayList<>(userMap.values());
        return values;
    }

    @ApiOperation(value = "Create User", notes = "Create user info according to User")
    @ApiImplicitParam(name = "user", value = "Entity user", required = true, dataType = "User")
    @RequestMapping(value = "", method = RequestMethod.POST)
    public String postUser(@RequestBody User user){
        userMap.put(user.getId(), user);
        return "success";
    }

    @ApiOperation(value = "Get User info", notes = "Get User Info detail according to url id")
    @ApiImplicitParam(name = "id", value = "User id", required = true, dataType = "Long", paramType = "path")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User getUser(@PathVariable Long id){
        return userMap.get(id);
    }

    @ApiOperation(value = "Update User Info", notes = "Appoint updating object according to url id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "User Id", required = true, dataType = "Long", paramType = "path"),
            @ApiImplicitParam(name = "user", value = "entity user", required = true, dataType = "User")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String putUser(@PathVariable Long id, @RequestBody User user){
        User u = userMap.get(id);
        u.setName(user.getName());
        u.setAge(user.getAge());
        userMap.put(id, u);
        return "success";
    }

    @ApiOperation(value = "Delete", notes = "delete user")
    @ApiImplicitParam(name = "id", value = "User Id", required = true, dataType = "Long", paramType = "path")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String deleteUser(@PathVariable Long id){
        userMap.remove(id);
        return "success";
    }
}
