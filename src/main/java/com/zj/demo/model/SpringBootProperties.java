package com.zj.demo.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SpringBootProperties {

    @Value("${com.jz.springboot.name}")
    private String name;
    @Value("${com.jz.springboot.title}")
    private String title;
    @Value("${com.jz.springboot.desc}")
    private String desc;

    @Value("${com.jz.springboot.value}")
    private String value;
    @Value("${com.jz.springboot.number}")
    private Integer number;
    @Value("${com.jz.springboot.bigNumber}")
    private Long bigNumber;
    @Value("${com.jz.springboot.test1}")
    private Integer test1;
    @Value("${com.jz.springboot.test2}")
    private Integer test2;

    public Integer getTest1() {
        return test1;
    }

    public void setTest1(Integer test1) {
        this.test1 = test1;
    }

    public Integer getTest2() {
        return test2;
    }

    public void setTest2(Integer test2) {
        this.test2 = test2;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getBigNumber() {
        return bigNumber;
    }

    public void setBigNumber(Long bigNumber) {
        this.bigNumber = bigNumber;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
