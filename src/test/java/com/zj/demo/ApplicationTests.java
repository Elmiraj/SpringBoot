package com.zj.demo;

import com.zj.demo.model.SpringBootProperties;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class ApplicationTests {

	private MockMvc mockMvc;

	private static final Log log = LogFactory.getLog(ApplicationTests.class);

	@Autowired
	private SpringBootProperties springBootProperties;


	@Before
	public void setup(){
		mockMvc = MockMvcBuilders.standaloneSetup(new Application()).build();
	}

	@Test
	public void getHello() throws Exception{
		mockMvc.perform(MockMvcRequestBuilders.get("/hello").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string(equalTo("Hello World")));
	}

	@Test
	public void getProperties() throws Exception{
		Assert.assertEquals("jz",springBootProperties.getName());
		Assert.assertEquals("springboot",springBootProperties.getTitle());
		Assert.assertEquals("jz is doing springboot"
				, springBootProperties.getDesc());
	}

	@Test
	public void random() throws Exception{
		log.info("Random number test: ");
		log.info("Random string: " + springBootProperties.getValue());
		log.info("Random int: " + springBootProperties.getNumber());
		log.info("Random bigNumber: " + springBootProperties.getBigNumber());
		log.info("Random within 10: " + springBootProperties.getTest1());
		log.info("Random between 10 and 20: " + springBootProperties.getTest2());
	}
}
