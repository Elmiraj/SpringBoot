package com.zj.demo;

import com.zj.demo.controller.UserController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserApplicationTest {
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception{
        mockMvc = MockMvcBuilders.standaloneSetup(new UserController()).build();
    }

    @Test
    public void testUserController() throws Exception{
        RequestBuilder requestBuilder = null;

//        Use get to check the user list, expect to be null
        requestBuilder = get("/user/");
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[]")));

//        Post a user within the request
        requestBuilder = post("/user/")
                .param("id","0")
                .param("name","zj")
                .param("age","24");
        mockMvc.perform(requestBuilder)
                .andExpect(content().string(equalTo("success")));

//        Check the user list
        requestBuilder = get("/user/");
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[{\"id\":0,\"name\":\"zj\",\"age\":24}]")));

//        Use put to update the user whose id is 0
        requestBuilder = put("/user/0")
                .param("name","lh")
                .param("age","23");
        mockMvc.perform(requestBuilder)
                .andExpect(content().string(equalTo("success")));

//        Get user whose id is 0
        requestBuilder = get("/user/0");
        mockMvc.perform(requestBuilder)
                .andExpect(content().string(equalTo("{\"id\":0,\"name\":\"lh\",\"age\":23}")));

//        Delete user whose id is 0
        requestBuilder = delete("/user/0");
        mockMvc.perform(requestBuilder)
                .andExpect(content().string(equalTo("success")));

//        Get user list
        requestBuilder = get("/user/");
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[]")));


    }
}
